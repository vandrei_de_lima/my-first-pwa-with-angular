import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [],
  exports: [CommonModule, TranslateModule],
})
export class SharedModule {}
