import { ApiCountriesHttpService } from './../../services/api-countries-http.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  isEmpty,
  map,
  merge,
  Observable,
  startWith,
  switchMap,
} from 'rxjs';
import { ArticlesHttpService } from 'src/app/services/articles-http.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
})
export class HomeComponent implements OnInit {
  searchField = new FormControl();
  country: FormControl = new FormControl();
  countryFilter: FormControl = new FormControl();
  filteredOptions!: Observable<any[]>;
  allActions$ = this.articleServices.getArticles();
  filterForSearch$ = this.searchField.valueChanges.pipe(
    debounceTime(300),
    filter((textSearch) => textSearch.length > 2 || !textSearch.length),
    distinctUntilChanged(),
    switchMap((textSearch) => {
      this.prepareLoading();
      return this.articleServices.getArticles(
        textSearch,
        this.country.value?.code
      );
    })
  );
  filterForCountries$ = this.country.valueChanges.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    filter((value) => value),
    switchMap((value) => {
      this.prepareLoading();
      return this.articleServices.getArticles(
        this.searchField.value || 'developer',
        value.code
      );
    })
  );

  articles$ = merge(
    this.allActions$,
    this.filterForSearch$,
    this.filterForCountries$
  );

  panelOpenState: boolean = true;
  isEmpty: boolean = false;
  isLoading: boolean = true;

  deferredPrompt: any;

  listCountires: any[] = [];

  constructor(
    public articleServices: ArticlesHttpService,
    private apiCountries: ApiCountriesHttpService,
    private translate: TranslateService
  ) {
    setTimeout(() => {
      this.panelOpenState = false;
    }, 1500);
  }

  ngOnInit(): void {
    window.addEventListener('beforeinstallprompt', (e) => {
      this.deferredPrompt = e;
    });

    this.apiCountries.getList().subscribe((result: any) => {
      this.listCountires = result;
      this.filteredOptions = this.countryFilter.valueChanges.pipe(
        startWith(''),
        map((value) => this.filter(value))
      );
    });

    this.articles$.subscribe((result) => {
      if (!result.length) {
        this.isEmpty = true;
        this.isLoading = false;
      }
      if (result.length) {
        this.isEmpty = false;
        this.isLoading = false;
      }
    });
  }

  prepareLoading(): void {
    this.isLoading = true;
    this.isEmpty = false;
  }

  filter(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.listCountires.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  donwload(): void {
    this.deferredPrompt.prompt();
  }

  goToLinkdin() {
    window.open('https://www.linkedin.com/in/vandrei-de-lima-87022b126/');
  }

  goGitLab() {
    window.open('https://gitlab.com/vandrei_de_lima');
  }

  changeLenguage(lenguage: string): void {
    this.translate.use(lenguage);
  }

  openArticle(article: any): void {
    console.log(article);
    window.open(article.link);
  }
}
