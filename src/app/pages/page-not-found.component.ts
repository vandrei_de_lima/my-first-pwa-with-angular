import { Component } from '@angular/core';

@Component({
  template: `<div class="wrapper__empty">
    <div>
      <h1>404 Ops..</h1>
      <span>Pagina não encontrada, verifique a sua conexão</span>
    </div>
  </div>`,
  styles: [
    `
      .wrapper__empty {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100vh;

        color: #fff;
        background: purple;
      }
    `,
  ],
})
export class NotFoundPageComponent {}
