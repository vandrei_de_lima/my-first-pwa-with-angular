import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { ArticlesHttpService } from './articles-http.service';

describe('ArticlesHttpService', () => {
  let service: ArticlesHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient],
    });
    service = TestBed.inject(ArticlesHttpService);
  });

  it(`${ArticlesHttpService.prototype.getArticles.name}should return articles when request is called`, (done) => {
    service.getArticles().subscribe((result) => {
    
      expect(result).toBeTruthy();
      done();
    });
  });

  it(`${ArticlesHttpService.prototype.getArticles.name}should return articles when request is called with text`, (done) => {
    service.getArticles('developer').subscribe((result) => {
  
      expect(result).toBeTruthy();
      done();
    });
  });
});
