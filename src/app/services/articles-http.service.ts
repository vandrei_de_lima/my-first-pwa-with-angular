import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, pluck } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ArticlesHttpService {
  private key: string = 'pub_45378972192fbfddbba52a94be03dea88e81';

  constructor(private http: HttpClient) {}

  getArticles(textSearch: string = 'developer', region: string = '') {
    const country = this.getCountry(region);

    const url: string = `https://newsdata.io/api/1/news?apikey=${this.key}&q=${textSearch}${country}`;

    return this.http.get<any>(url).pipe(
      pluck('results'),
      map((results) => this.prepareArticles(results))
    );
  }

  private getCountry(region: string): string {
    if (!region) return '';

    return `&country=${region}`;
  }

  private prepareArticles(result: any[]): any[] {
    let data: any[] = [];
    data = result.map((article) => {
      if (article.description)
        article.description = `${article.description.substr(0, 100)}...`;

      return article;
    });
    return data;
  }
}
