import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { COUNTRY_API_FILTER } from './const';

@Injectable({
  providedIn: 'root',
})
export class ApiCountriesHttpService {
  constructor(private http: HttpClient) {}

  getList() {
    const url = 'https://api.coindirect.com/api/country?max=200';

    return this.http.get<any[]>(url).pipe(
      map((results: any) => {
        let result = results
          .filter((result: any) => COUNTRY_API_FILTER.includes(result.name))
          .map((result: any) => {
            return { code: result.code.toLowerCase(), name: result.name };
          });

        return [{ code: '', name: '' }, ...result];
      })
    );
  }
}
