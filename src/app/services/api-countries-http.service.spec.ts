import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { ApiCountriesHttpService } from './api-countries-http.service';

describe('ApiCountriesHttpService', () => {
  let service: ApiCountriesHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient],
    });
    service = TestBed.inject(ApiCountriesHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
